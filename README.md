## Read Write Memory

#### Classe que realiza a leitura e a grava��o em determinados endere�os de mem�ria

Primeiro devemos procurar pelo processo
```csharp
 Process[] processes = Process.GetProcessesByName("NomeDoProcesso");
```
Verificar se o processo � valido
```csharp
 Process[] processes = Process.GetProcessesByName("NomeDoProcesso");
 if (processes.Length == 0)
 {
	MessageBox.Show("Erro ao abrir o processo, reinicie e tente novamente o jogo est� aberto?", "Program");              
 }
```
Se condi��o for falsa iremos agora inst�nciar a classe **Memory** e como par�metro o processo
```csharp
Process[] processes = Process.GetProcessesByName("NomeDoProcesso");
 if (processes.Length == 0)
 {
	MessageBox.Show("Erro ao abrir o processo, reinicie e tente novamente o jogo est� aberto?", "Program");              
 }
 else
 {
	using (rw.Memory memory = new rw.Memory(processes[0]))
    {
 
    }
 }
    
```
Inst�ncia criada vamos agora criar uma vari�vel que armazenar� o endere�o de mem�ria chamada **address** do tipo **IntPtr** e chamar pelo m�todo **memory.getAddress();** em que passaremos como par�metro um Pointer e devolvera o endere�o de mem�ria que esse Pointer est� apontando.
```csharp
Process[] processes = Process.GetProcessesByName("NomeDoProcesso");
 if (processes.Length == 0)
 {
	MessageBox.Show("Erro ao abrir o processo, reinicie e tente novamente o jogo est� aberto?", "Program");              
 }
 else
 {
	using (rw.Memory memory = new rw.Memory(processes[0]))
    {
    	IntPtr address;
        address = memory.GetAddress("Modulo.exe", (IntPtr)0x006DC9A0, new int[] { 0xF0, 0xF4, 0x134, 0x3C, 0x4 });
    }
 }
    
```
Se bem sucedido agora s� chamar pelos m�todos da classe **Memory** Read e Write

```csharp
Process[] processes = Process.GetProcessesByName("NomeDoProcesso");
 if (processes.Length == 0)
 {
	MessageBox.Show("Erro ao abrir o processo, reinicie e tente novamente o jogo est� aberto?", "Program");              
 }
 else
 {
	using (rw.Memory memory = new rw.Memory(processes[0]))
    {
    	IntPtr address;
        address = memory.GetAddress("Modulo.exe", (IntPtr)0xAABBCCDD, new int[] { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE });
        String valorASerLido;
        valorASerLido = memory.ReadFloat(address).ToString();
        
        memory.WriteFloat(address, 100000);
    }
 }
    
```
